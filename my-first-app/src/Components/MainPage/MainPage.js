import React, {Component} from 'react';
import ProductList from "../ProductList/ProductList";
import ModalWindow from "../ModalWindow/ModalWindow";
import Button from "../Button/Button";
import "./MainPage.scss"


class MainPage extends Component {

    state = {
        modalWindow: false,
        products : [],
        added:[],
        favorites:[]
    };

    componentDidMount() {
        fetch("products.json")
            .then(r => r.json())
            .then(
                (data) => {
                    this.setState({products: data});
                },
            );

        localStorage.setItem('added', JSON.stringify([]));
        localStorage.setItem('favorites', JSON.stringify([]));



    }


    toggleModalWindow = () =>{
        this.setState({modalWindow :!this.state.modalWindow});
    };

    addToCart = (id) =>{
        this.toggleModalWindow();
        this.state.added.push(this.state.products.filter(product => product.number === id));
        console.log(id);
        localStorage.setItem("added", JSON.stringify(this.state.added));

    };
    addToFavorites =(id) =>{
        console.log(id);
        this.state.favorites.push(this.state.products.filter(product => product.number === id));
        localStorage.setItem("favorites", JSON.stringify(this.state.favorites));
    };

    render() {
        return (
            <div className={"main-page"}>
                 <ProductList  clicked={this.toggleModalWindow}
                               products={this.state.products}
                               addToCart={this.addToCart}
                               addToFavorites={this.addToFavorites}
                 />
                 {

                this.state.modalWindow ?
                    <ModalWindow closed={this.toggleModalWindow}
                                 windowtext={"Do you want to add this product to cart?"}
                                 headertext={"Add To Cart"}
                                 action={[<Button text={"Add"} clicked={this.toggleModalWindow} />,
                                     <Button text={"Cancel"} clicked={this.toggleModalWindow}/>
                                     ]}
                    /> : null
                }

            </div>

        );
    }
}

export default MainPage;